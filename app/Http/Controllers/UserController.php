<?php
namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Product;
use App\Follow;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Session;
use Carbon\Carbon;

class UserController extends Controller
{



    public function postChangeEmail(Request $request){
    $this->validate($request, [
        'email' => 'email|required'
    ]);
        $user=Auth::user();
        $user->email=$request['email'];
        $user->save();
        Auth::logout();
        return redirect()->route('user.signin')->with('success', "E-mail adresiniz başarıyla değiştirildi. Tekrar giriş yapmalısınız.");
    }
    public function postChangePass(Request $request){
    $this->validate($request, [
        'password' => 'required|min:4'
    ]);
    $user=Auth::user();
    $user->password=bcrypt($request['password']);
    $user->save();
    Auth::logout();
    return redirect()->route('user.signin')->with('success', "Şifreniz başarıyla değiştirildi. Tekrar giriş yapmalısınız.");
    }



    public function notify(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->limit(6)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $f_price = (($product->price - $product->minprice) / 2) + $product->minprice;

            if ($product->current_price < $f_price) {
                $i_urunler[] = $product;
            }
        }

        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if($follow->seen < 1 && $id=="1"){
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                    $follow->seen = 1;
                    $follow->save();
                }}
                else if($follow->seen < 1 && $id=="0"){
                    if ($follow->product_id == $i_urun->id) {
                        $b_urunler[] = $i_urun;
                        $follow->save();
                }}
            }
        }
        $json_list = json_encode($b_urunler);
        return response()->json($json_list);
    }
    
    public function getSignup()
    {
        return view('user.signup');
    }

    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);
        $user = new User([
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);
        $user->save();
        Auth::login($user);

        if (Session::has('oldUrl')) {
            $oldUrl = Session::get('oldUrl');
            Session::forget('oldUrl');
            return redirect()->to($oldUrl);
        } else {
            return redirect()->route('product.index');
        }
    }

    public function getSignin()
    {
        return view('user.signin');
    }

    public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);
        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ])) {
            if (Session::has('oldUrl')) {
                $oldUrl = Session::get('oldUrl');
                Session::forget('oldUrl');
                return redirect()->to($oldUrl);
            } else {
                return redirect()->route('user.profile');
            }
        }
        return redirect()->back()->with('uyari', 'E-mail veya şifreniz hatalı.');
    }

    public function getProfile()
    {
        $orders = Auth::user()->orders;
        $orders->transform(function ($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });

        $user_id = Auth::user()->id;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $i_urunler[] = $product;
        }
        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                }
            }
        }

        return view('user.profile', [
            'orders' => $orders,
            'b_urunler' => $b_urunler
        ]);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('product.index');
    }

    public function getNotification()
    {
        $user_id = Auth::user()->id;
        $products = Product::all();
        $follows = Follow::where('user_id', $user_id)->get();
        $i_urunler = array();
        $b_urunler = array();
        foreach ($products as $product) {
            $f_price = (($product->price - $product->minprice) / 2) + $product->minprice;

            if ($product->current_price < $f_price) {
                $i_urunler[] = $product;
            }
        }

        foreach ($follows as $follow) {
            foreach ($i_urunler as $i_urun) {
                if ($follow->product_id == $i_urun->id) {
                    $b_urunler[] = $i_urun;
                }
            }
        }
        
        return view('user.bildirim', [
            'b_urunler' => $b_urunler
        ]);
    }
}
