<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Cart;
use App\Product;
use App\Order;
use App\Cat;
use App\Subcat;
use App\Comment;
use App\Follow;
use Auth;
use Faker\Provider\tr_TR\DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use Session;
use Carbon\Carbon;



use DB;

class AdminController extends Controller
{


  public function getAddProduct(){
      $cats=Cat::all();
      $brands=Brand::all();
      //return view('admin.urunekle', ['cats'=>$cats, 'subcats'=>$subcats]);
      return view('admin.urunekle', ['cats'=>$cats,'brands'=>$brands]);
  }

  public function subcats(Request $request, $id){
      $cats_id = $id;
      $subcats = Subcat::where('cat_id', '=', $cats_id)->get();
      return response()->json($subcats);
  }

public function getAddBrand(){
  return view('admin.markaekle');
}
  public function getEditBrand($id){
    $brand=Brand::find($id);
    return view('admin.markaduzenle',['brand'=>$brand]);
  }
    public function getRemoveBrand($id){
    $brand=Brand::find($id);
      return view('admin.markasil',['brand'=>$brand]);
    }



    public function postAddBrand(Request $request){

          $this->validate($request,[
              'name' => 'required|unique:brands',

          ]);
          $brand = new Brand();
          $brand->name = $request['name'];

          $brand->save();

          return redirect()->route('admin.panel')->with('success', 'Marka başarıyla eklendi.');
    }

      public function postEditBrand(Request $request){
        $this->validate($request,[
            'name' => 'required'

        ]);
        $brand =Brand::find($request['id']);
        $brand->name = $request['name'];

        $brand->save();

        return redirect()->route('admin.panel')->with('success', 'Marka başarıyla düzenlendi.');

      }

        public function postRemoveBrand(Request $request){
          $products=Product::where('brand_id','=',$request['id'])->get();
          foreach($products as $product){
            if($product->imageName != "default.jpg"){
            Storage::disk('public')->delete($product->imageName);
            }
            if($product->image1 != "default.jpg"){
            Storage::disk('public')->delete($product->image1);
            }
            if($product->image2 != "default.jpg"){
            Storage::disk('public')->delete($product->image2);
            }
            if($product->image3 != "default.jpg"){
            Storage::disk('public')->delete($product->image3);
            }
            Comment::where('product_id','=',$product->id)->delete();
            Follow::where('product_id','=',$product->id)->delete();
}


          Product::where('brand_id','=',$request['id'])->delete();
          Brand::where('id','=',$request['id'])->delete();

          return redirect()->route('admin.panel')->with('success', 'Marka başarıyla kaldırıldı.');

        }



  public function postAddProduct(Request $request)
  {
      $this->validate($request,[
          'title' => 'required|unique:products',
          'description' => 'required',
          'qty' => 'required|numeric|min:1',
          'productImage' => 'mimes:jpeg,jpg,png,svg|max:1000',
          'image1' => 'mimes:jpeg,jpg,png,svg|max:1000',
          'image2' => 'mimes:jpeg,jpg,png,svg|max:1000',
          'image3' => 'mimes:jpeg,jpg,png,svg|max:1000',
          'price' => 'required|numeric',
		  'minprice' => 'required|numeric',
		  'end_date' => 'required|date|after:yesterday',
          'end_zaman' => 'required',
          'brand_id' => 'required'
      ]);
      $product = new Product();
      $product->brand_id = $request['brand_id'];
      $product->title = $request['title'];
      $product->description = $request['description'];
      $product->rich_description = $request['rich_description'];
      $product->qty = $request['qty'];
	  $product->cat_id = $request['cat_id'];
	  $product->subcat_id = $request['subcat_id'];
	  $now = Carbon::now();
	  $product->begin_date = $now;
    $endDate = new Carbon($request['end_date']);
	  $testDate = new Carbon($request['end_zaman']);
    $endDate->addHours($testDate->hour);
   $endDate->addMinutes($testDate->minute);
   $endDate->addSeconds($testDate->second);
	  $product->end_date = $endDate;
      $product->price = $request['price'];
      $product->current_price = $request['price'];
	  $product->minprice = $request['minprice'];
      $fileProductImage = $request->file('productImage');
      $fileImage1 = $request->file('image1');
      $fileImage2 = $request->file('image2');
      $fileImage3 = $request->file('image3');
      if ($fileProductImage) {
          $fileNameForProductImage = $request['title'] . File::extension($fileProductImage);
          $product->imageName = $fileNameForProductImage;
          Storage::disk('public')->put($fileNameForProductImage, File::get($fileProductImage));
      }
      if ($fileImage1) {
          $fileNameForImage1 = $request['title'] ."-1". File::extension($fileImage1);
          $product->image1 = $fileNameForImage1;
          Storage::disk('public')->put($fileNameForImage1, File::get($fileImage1));
      }
      if ($fileImage2) {
          $fileNameForImage2= $request['title'] ."-2". File::extension($fileImage2);
          $product->image2 = $fileNameForImage2;
          Storage::disk('public')->put($fileNameForImage2, File::get($fileImage2));
      }
      if ($fileImage3) {
          $fileNameForImage3 = $request['title']."-3" . File::extension($fileImage3);
          $product->image3 = $fileNameForImage3;
          Storage::disk('public')->put($fileNameForImage3, File::get($fileImage3));
      }
      $product->save();

      return redirect()->route('admin.panel')->with('success', 'Ürün başarıyla eklendi.');
  }



  public function getAddCat(){
      return view('admin.kategoriekle');
  }




  public function postAddCat(Request $request)
  {
      $this->validate($request,[
          'name' => 'required|unique:cats',

      ]);
      $cat = new Cat();
      $cat->name = $request['name'];

      $cat->save();

      return redirect()->route('admin.panel')->with('success', 'Kategori başarıyla eklendi.');
  }

  public function getAddSubCat(){
    $cats = Cat::all();
      return view('admin.altkategoriekle', ['cats' => $cats]);
  }




  public function postAddSubCat(Request $request)
  {
      $this->validate($request,[
          'name' => 'required|unique:cats',

      ]);

      $subcat = new Subcat();
      $subcat->name = $request['name'];
      $subcat->cat_id = $request['id'];

      $subcat->save();

      return redirect()->route('admin.panel')->with('success', 'Alt Kategori başarıyla eklendi.');
  }



  public function getRemoveProduct($id){
$product=Product::find($id);
    return view('admin.urunsil',['product'=>$product]);

    }

  public function postRemoveProduct(Request $request){

    Comment::where('product_id','=',$request['id'])->delete();
    Follow::where('product_id','=',$request['id'])->delete();
    $product=Product::find($request['id']);
    Product::where('id','=',$request['id'])->delete();
    if($product->imageName != "default.jpg"){
    Storage::disk('public')->delete($product->imageName);
    }
    if($product->image1 != "default.jpg"){
    Storage::disk('public')->delete($product->image1);
    }
    if($product->image2 != "default.jpg"){
    Storage::disk('public')->delete($product->image2);
    }
    if($product->image3 != "default.jpg"){
    Storage::disk('public')->delete($product->image3);
    }



    return redirect()->route('admin.panel')->with('success', 'Ürün başarıyla kaldırıldı.');

  }


  public function getRemoveCat($id){
      $cat=Cat::find($id);
    return view('admin.kategorisil',['cat'=>$cat]);

    }

  public function postRemoveCat(Request $request){
    $products=Product::where('cat_id','=',$request['id'])->get();
    foreach($products as $product){
      if($product->imageName != "default.jpg"){
    Storage::disk('public')->delete($product->imageName);
  }
  if($product->image1 != "default.jpg"){
    Storage::disk('public')->delete($product->image1);
  }
  if($product->image2 != "default.jpg"){
    Storage::disk('public')->delete($product->image2);
  }
  if($product->image3 != "default.jpg"){
    Storage::disk('public')->delete($product->image3);
  }

  Comment::where('product_id','=',$product->id)->delete();
  Follow::where('product_id','=',$product->id)->delete();



}
    Product::where('cat_id','=',$request['id'])->delete();
    Subcat::where('cat_id','=',$request['id'])->delete();
    Cat::where('id','=',$request['id'])->delete();


    return redirect()->route('admin.panel')->with('success', 'Kategori ve ait alt kategoriler başarıyla kaldırıldı.');

  }

  public function getRemoveSubcat($id){
      $subcat=Subcat::find($id);
    return view('admin.altkategorisil',['subcat'=>$subcat]);

    }

  public function postRemoveSubcat(Request $request){
    $products=Product::where('subcat_id','=',$request['id'])->get();
    foreach($products as $product){
      if($product->imageName != "default.jpg"){
    Storage::disk('public')->delete($product->imageName);
  }
  if($product->image1 != "default.jpg"){
    Storage::disk('public')->delete($product->image1);
  }
  if($product->image2 != "default.jpg"){
    Storage::disk('public')->delete($product->image2);
  }
  if($product->image3 != "default.jpg"){
    Storage::disk('public')->delete($product->image3);
  }

  Comment::where('product_id','=',$product->id)->delete();
  Follow::where('product_id','=',$product->id)->delete();
}




    Product::where('subcat_id','=',$request['id'])->delete();
    Subcat::where('id','=',$request['id'])->delete();

    return redirect()->route('admin.panel')->with('success', 'Alt kategori başarıyla kaldırıldı.');

  }

public function getEditProduct($id){
  $cats=Cat::all();
  $product=Product::find($id);
  $brands=Brand::all();
  return view('admin.urunduzenle',['cats'=>$cats, 'product'=>$product,'brands'=>$brands]);
}

public function postEditProduct(Request $request){
    $this->validate($request,[
        'title' => 'required',
        'description' => 'required',
        'qty' => 'required|numeric|min:1',
        'productImage' => 'mimes:jpeg,jpg,png,svg|max:1000',
        'image1' => 'mimes:jpeg,jpg,png,svg|max:1000',
        'image2' => 'mimes:jpeg,jpg,png,svg|max:1000',
        'image3' => 'mimes:jpeg,jpg,png,svg|max:1000',
        'price' => 'required|numeric',
        'minprice' => 'required|numeric',
        'end_date' => 'required|date|after:yesterday',
        'end_zaman' => 'required',
        'brand_id' => 'required'
    ]);
    $product = Product::find($request['id']);
    $product->brand_id = $request['brand_id'];
    $product->title = $request['title'];
    $product->description = $request['description'];
    $product->rich_description = $request['rich_description'];
    $product->qty = $request['qty'];
    $product->cat_id = $request['cat_id'];
    $product->subcat_id = $request['subcat_id'];
    $now = Carbon::now();
    $product->begin_date = $now;
    $endDate = new Carbon($request['end_date']);
    $testDate = new Carbon($request['end_zaman']);
    $endDate->addHours($testDate->hour);
$endDate->addMinutes($testDate->minute);
$endDate->addSeconds($testDate->second);
    $product->end_date = $endDate;
    $product->price = $request['price'];
    $product->current_price = $request['price'];
    $product->minprice = $request['minprice'];
    $fileProductImage = $request->file('productImage');
    $fileImage1 = $request->file('image1');
    $fileImage2 = $request->file('image2');
    $fileImage3 = $request->file('image3');
    if ($fileProductImage) {
        $fileNameForProductImage = $request['title'] . File::extension($fileProductImage);
        $product->imageName = $fileNameForProductImage;
        Storage::disk('public')->put($fileNameForProductImage, File::get($fileProductImage));
    }
    if ($fileImage1) {
        $fileNameForImage1 = $request['title'] ."-1". File::extension($fileImage1);
        $product->image1 = $fileNameForImage1;
        Storage::disk('public')->put($fileNameForImage1, File::get($fileImage1));
    }
    if ($fileImage2) {
        $fileNameForImage2= $request['title'] ."-2". File::extension($fileImage2);
        $product->image2 = $fileNameForImage2;
        Storage::disk('public')->put($fileNameForImage2, File::get($fileImage2));
    }
    if ($fileImage3) {
        $fileNameForImage3 = $request['title']."-3" . File::extension($fileImage3);
        $product->image3 = $fileNameForImage3;
        Storage::disk('public')->put($fileNameForImage3, File::get($fileImage3));
    }
    $product->save();

    return redirect()->route('admin.panel')->with('success', 'Ürün başarıyla düzenlendi.');
}


public function getEditCat($id){
  $cat=Cat::find($id);
  return view('admin.anakategoriduzenle',['cat'=>$cat]);
}

public function postEditCat(Request $request)
{
    $this->validate($request,[
        'name' => 'required'

    ]);
    $cat =Cat::find($request['id']);
    $cat->name = $request['name'];

    $cat->save();

    return redirect()->route('admin.panel')->with('success', 'Kategori başarıyla düzenlendi.');
}

public function getEditSubcat($id){
  $cats=Cat::all();
  $subcat=Subcat::find($id);
  return view('admin.altkategoriduzenle',['cats'=>$cats, 'subcat'=>$subcat]);
}

public function postEditSubcat(Request $request)
{
    $this->validate($request,[
        'name' => 'required',
        'cat_id' => 'required'

    ]);
    //$products=Product::where('subcat_id','=',$request['id']);
    $subcat=Subcat::find($request['id']);
    $subcat->name=$request['name'];
    $subcat->cat_id=$request['cat_id'];
    foreach ($subcat->products as $product){
      $product->cat_id=$request['cat_id'];
      $product->save();
    }



    $subcat->save();


    return redirect()->route('admin.panel')->with('success', 'Alt kategori başarıyla düzenlendi.');
}


public function getAdminPanel(){
  $orders=Order::all();
  $brands=brand::all();
  $cats=Cat::all();
  $subcats=Subcat::all();
  $products=Product::all();

  $orders->transform(function($order,$key){
    $order->cart=unserialize($order->cart);
    return $order;
  });

  return view('admin.panel',['products'=>$products,'subcats'=>$subcats,'cats'=>$cats,'orders'=>$orders,'brands'=>$brands]);
}



}
