<?php

use Illuminate\Database\Seeder;
use App\Brand;
use App\Product;
use App\Cat;
use App\Subcat;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product([
        		'title' => 'Harry Potter',
        		'description' => 'Super cool - at least as a child',
                'rich_description' => 'Çok önemli rich desc',
                'qty' => 5,
                'price' => 10,
                'current_price' => 10,
				'minprice' => 5,
                'cat_id' => 1,
                'subcat_id' => 2,
                'brand_id' => 1,
                'begin_date' => new DateTime(),
                'end_date' => new DateTime()

        ]);
        $product->save();

        $product = new Product([
        		'title' => 'Kitap 2',
                'description' => 'Super cool - at least as a child',
                'rich_description' => 'Çok önemli rich desc',
                'qty' => 5,
                'price' => 10,
                'current_price' => 10,
				'minprice' => 5,
                'cat_id' => 1,
                'subcat_id' => 2,
                'brand_id' => 2,
                'begin_date' => new DateTime(),
                'end_date' => new DateTime()
        ]);
        $product->save();

        $product = new Product([
        		'title' => 'Kitap 3',
                'description' => 'Super cool - at least as a child',
                'rich_description' => 'Çok önemli rich desc',
                'qty' => 5,
                'price' => 10,
                'current_price' => 10,
				'minprice' => 5,
                'cat_id' => 1,
                'subcat_id' => 1,
                'brand_id' => 2,
                'begin_date' => new DateTime(),
                'end_date' => new DateTime()
        ]);
        $product->save();

        $product = new Product([
                'title' => 'Kitap 4',
                'description' => 'Super cool - at least as a child',
                'rich_description' => 'Çok önemli rich desc',
                'qty' => 5,
                'price' => 10,
                'current_price' => 10,
				'minprice' => 5,
                'cat_id' => 1,
                'subcat_id' => 1,
                'brand_id' => 3,
                'begin_date' => new DateTime(),
                'end_date' => new DateTime()
        ]);
        $product->save();

        $cat = new Cat([
                'name' => 'Kategori 1',
        ]);
        $cat->save();

        $cat = new Cat([
                'name' => 'Kategori 2',
        ]);
        $cat->save();

        $cat = new Cat([
                'name' => 'Kategori 3',
        ]);
        $cat->save();

        $cat = new Cat([
                'name' => 'Kategori 4',
        ]);
        $cat->save();

        $subcat = new Subcat([
                'name' => 'Alt Kategori 1',
                'cat_id'=>1
        ]);
        $subcat->save();
        $subcat = new Subcat([
                'name' => 'Alt Kategori 2',
                'cat_id'=>2
        ]);
        $subcat->save();

        $subcat = new Subcat([
                'name' => 'Alt Kategori 3',
                'cat_id'=>3
        ]);
        $subcat->save();

        $subcat = new Subcat([
                'name' => 'Alt Kategori 4',
                'cat_id'=>4
        ]);
        $subcat->save();
        $brand = new Brand([
            'name' => 'Brand 1'
        ]);
        $brand->save();
        $brand = new Brand([
            'name' => 'Brand 2'
        ]);
        $brand->save();
        $brand = new Brand([
            'name' => 'Brand 3'
        ]);
        $brand->save();
        $brand->save();
        $brand = new Brand([
            'name' => 'Brand 4'
        ]);
        $brand->save();
        $brand->save();
        $brand = new Brand([
            'name' => 'Brand 5'
        ]);
        $brand->save();
    }
}
