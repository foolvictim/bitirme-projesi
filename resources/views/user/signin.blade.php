@extends('layouts.master')

@section('title')
    Üye Girişi
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h1>Giriş Yap</h1>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
@if(Session::has('uyari'))
<div class="alert alert-danger">
<p>{{ Session::get('uyari')}}</p>
</div>
@endif
@if(Session::has('success'))
<div class="alert alert-success">
<p>{{ Session::get('success')}}</p>
</div>
@endif
<form action="{{route('user.signin')}}" method="post">
<div class="form-group">
<label for="email">E-Mail</label>
<input type="text" id="email" name="email" class="form-control">
</div>
<div class="form-group">
<label for="password">Şifre</label>
<input type="password" id="password" name="password" class="form-control">
</div>
<button type="submit" class="btn btn-primary">Giriş Yap</button>
{{csrf_field()}}
</form>
<p>Henüz hesabınız yok mu? <a href="{{route('user.signup')}}">Kayıt Olmayı Deneyin! </a></p>
  </div>
</div>

@endsection
