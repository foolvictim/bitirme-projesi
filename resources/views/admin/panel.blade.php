@extends('layouts.master')

@section('title')
    Admin Kontrol Paneli
@endsection

@section('content')
<h1>Admin Paneli</h1>
<hr>
@if(Session::has('success'))
   <div class="row">
     <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
       <div id="charge-message" class="alert alert-success">
         {{ Session::get('success') }}
       </div>
     </div>
   </div>
@endif

  <div class="category-tab shop-details-tab"><!--category-tab-->
    <div class="col-sm-12">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#products" data-toggle="tab">Ürünler</a></li>
        <li ><a href="#brands" data-toggle="tab">Markalar</a></li>
        <li><a href="#cats" data-toggle="tab">Kategoriler</a></li>
        <li><a href="#subcats" data-toggle="tab">Alt Kategoriler</a></li>
        <li><a href="#orders" data-toggle="tab">Gelen Siparişler</a></li>
      </ul>
    </div>
    <div class="tab-content">



      <div class="tab-pane fade active in col-cm-8 col-sm-offset-1" id="products" >
<h1><a href="{{route('admin.addProduct')}}"><i class="fa fa-file-text pull-right" aria-hidden="true"> Yeni Ürün</i></a>Ürünler</h1>
        <section id="cart_items">
      		<div class="container col-sm-11">
        <div class="table-responsive cart_info">
          <table class="table table-condensed">
            <thead>
              <tr class="cart_menu">
                <td class="image">Ürün Fotoğrafı</td>
                <td class="description">Ürün Adı</td>
                <td class="price">Ürün Fiyatı</td>
                <td class="quantity">Düzenle</td>
                <td class="total">Sil</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
              <tr>
                <td class="cart_product">
                    <img class="image-responsive" width="50" height="50" src="{{ route('product.image', ['filename' => $product->imageName]) }}" alt="Card image cap">

                </td>
                <td class="cart_description">
                  <big><strong>  {{$product->title}} </strong></big>
                </td>
                <td class="cart_delete">
                  <p class="cart_total_price">{{ number_format($product['price'], 2, ',', '.') }}<i class="fa fa-try" aria-hidden="true"></i></p>
                </td>
                <td class="cart_total">
                  <a href="{{ URL::to('urunduzenle/'.$product->id) }}"><i class="fa fa-pencil-square-o pull-right fa-2x" aria-hidden="true"></i></a>
                </td>
                <td class="cart_quantity">
                  <a href="{{ URL::to('urunsil/'.$product->id) }}"><i class="fa fa-trash pull-right fa-2x" aria-hidden="true"></i></a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
      </section>

      </div>

<div class="tab-pane fade" id="brands" >

  <h1><a href="{{route('admin.addBrand')}}"><i class="fa fa-file-text pull-right" aria-hidden="true"> Yeni Marka</i></a>Markalar</h1>
@foreach($brands as $brand)
  <nav aria-label="breadcrumb" role="navigation">
    <ol class="breadcrumb">
      <a href="{{ URL::to('markaduzenle/'.$brand->id) }}"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
      <a href="{{ URL::to('markasil/'.$brand->id) }}"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
      <li class="breadcrumb-item">{{$brand->name}}</li>

    </ol>
  </nav>
  @endforeach

</div>

      <div class="tab-pane fade" id="cats" >

        <h1><a href="{{route('admin.addCat')}}"><i class="fa fa-file-text pull-right" aria-hidden="true"> Yeni Kategori</i></a>Ana Kategoriler</h1>
  @foreach($cats as $cat)
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <a href="{{ URL::to('anakategoriduzenle/'.$cat->id) }}"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
            <a href="{{ URL::to('anakategorisil/'.$cat->id) }}"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
            <li class="breadcrumb-item">{{$cat->name}}</li>

          </ol>
        </nav>
        @endforeach


      </div>


      <div class="tab-pane fade" id="subcats" >

        <h1><a href="{{route('admin.addSubCat')}}"><i class="fa fa-file-text pull-right" aria-hidden="true"> Yeni Alt Kategori</i></a>Alt Kategoriler</h1>
    @foreach($subcats as $subcat)
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <a href="{{ URL::to('altkategoriduzenle/'.$subcat->id) }}"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
              <a href="{{ URL::to('altkategorisil/'.$subcat->id) }}"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;
              <li class="breadcrumb-item">{{$subcat->cat->name}}</li>
              <li class="breadcrumb-item">{{$subcat->name}}</li>
            </ol>
          </nav>
          @endforeach

      </div>


      <div class="tab-pane fade" id="orders" >







        <div id="accordion" role="tablist">
          @foreach($orders as $order)
          <div class="card">
            <div class="card-header" role="tab" id="headingOne">
              <h5 class="mb-0">
                <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  {{$order['name']}} isimli kişi tarafından {{$order['created_at']}} tarihinde siperiş edildi.
                </a>
              </h5>
            </div>

            <div id="{{$order['created_at']}}" class="collapse show" role="tabpanel" aria-labelledby="{{$order['created_at']}}" data-parent="#accordion">
              <div class="card-body">

                <ul class="list-group">
                  @foreach($order->cart->items as $item)
                  <li class="list-group-item">
        <span class="badge">{{$item['price']}} <i class="fa fa-try" aria-hidden="true"></i></span>
        {{$item['item']['title']}} ürününden, {{$item['qty']}} adet alındı.
                  </li>
                  @endforeach
                  <li class="list-group-item">
                    Gideceği Adres:  {{$order['address']}}
                  </li>
                  <li class="list-group-item">
                    İletişim Email:  {{$order->user->email}}
                  </li>


                  <li class="list-group-item list-group-item-secondary">Toplam Fiyat: {{ number_format($order->cart->totalPrice, 2, ',', '.') }} <i class="fa fa-try" aria-hidden="true"></i></li>
                </ul>

              </div>
            </div>
          </div>
        @endforeach
        </div>









      </div>

    </div>
  </div><!--/category-tab-->











@endsection
