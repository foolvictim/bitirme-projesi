@extends('layouts.master')

@section('title')
    Ürün Ekleme Ekranı
@endsection

@section('styles')
<link rel="stylesheet" href="{{URL::to('css/pickadate/classic.css')}}" >
<link rel="stylesheet" href="{{URL::to('css/pickadate/classic.date.css')}}" >
<link rel="stylesheet" href="{{URL::to('css/pickadate/classic.time.css')}}" >
<link rel="stylesheet" href="{{URL::to('css/wickedpicker.min.css')}}" >
<link rel="stylesheet" href="{{URL::to('css/bootstrap-timepicker.min.css')}}" >
@endsection
@section('content')
<div class="row">
<div class="col-sm-4 col-sm-offset-4">
<h1>Ürün Ekle</h1>
<hr>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
</div>
</div>
<div class="row">
<div class="col-sm-6 col-sm-offset-3">
<form action="{{ route('admin.addProduct') }}" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="title">Başlık</label>
			<input type="text" id="title" name="title" class="form-control">
		</div>
		<div class="form-group">
			<label for="description">Açıklama</label>
			<textarea name="description" id="description" class="form-control aciklama"></textarea>
		</div>
          <div class="form-group">
            <label for="">Kategori</label>
            <select class="form-control" name="cat_id" id="cats">
              <option value="0" disable="true" selected="true">=== Kategori Sec ===</option>
                @foreach ($cats as $key => $value)
                  <option value="{{$value->id}}">{{ $value->name }}</option>
                @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="">Alt Kategori</label>
            <select class="form-control" name="subcat_id" id="subcats">
              <option value="0" disable="true" selected="true">=== Alt Kategori Sec ===</option>
            </select>
          </div>
         <div class="form-group">
           <select name="brand_id">
               @foreach ($brands as $brand)
                 <option value="{{$brand->id}}">{{ $brand->name }}</option>
               @endforeach
           </select>
         
   		</div>
    <div class="form-group">
        <label for="rich_description">Geniş Açıklama</label>
        <textarea name="rich_description" id="rich_description" class="form-control genisAciklama"></textarea>
    </div>
    <div class="form-group">
        <label for="qty">Adet</label>
        <input type="text" id="qty" name="qty" class="form-control">
    </div>
    <div class="form-group">
        <label for="price">Başlangıç Fiyatı</label>
        <input type="text" id="price" name="price" class="form-control">
    </div>
    <div class="form-group">
        <label for="minprice">Minimum Fiyatı</label>
        <input type="text" id="minprice" name="minprice" class="form-control">
    </div>
    <div class="form-group">
        <label for="date">Bitiş Tarihi</label>
        <input type="text" id="date" name="end_date" class="form-control" placeholder="Tarih seçin">
    </div>
    <div class="input-group bootstrap-timepicker timepicker">
    	<label for="zaman">Bitiş Saati</label>
        <input type="text" id="zaman" name="end_zaman" class="form-control input-small">
        <span class="input-group-addon">
        <i class="fa fa-clock-o"></i>
        </span>
    </div>
    <div class="form-group">
      <div id="imageName-change"  style="background-image: url('{{ route('product.image', ['filename' => 'default.jpg']) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
    </div>
        <label for="productImage">Ürün Fotoğrafı</label>
        <input type="file" id="imageName-choose" name="productImage">
    </div>
    <div class="form-group">
      <div id="image1-change"  style="background-image: url('{{ route('product.image', ['filename' => 'default.jpg']) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
    </div>
        <label for="image1">Resim 1</label>
        <input type="file" id="image1-choose" name="image1">
    </div>
    <div class="form-group">
      <div id="image2-change"  style="background-image: url('{{ route('product.image', ['filename' => 'default.jpg']) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
    </div>
        <label for="image2">Resim 2</label>
        <input type="file" id="image2-choose" name="image2">
    </div>
    <div class="form-group">
      <div id="image3-change"  style="background-image: url('{{ route('product.image', ['filename' => 'default.jpg']) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
    </div>
        <label for="image3">Resim 3</label>
        <input type="file" id="image3-choose" name="image3">
    </div>
    <button type="submit" class="btn btn-primary">Ürün Ekle</button>
    {{csrf_field()}}
</form>
</div>
</div>



@endsection
@section('scripts')
    <script type="text/javascript">
	$("#cats").on("change", function(e) {
		console.log(e);
		var cat_id = e.target.value;
		console.log(cat_id);
        $.get('{{URL::to('json-subcats')}}/' + cat_id,function(data) {
          console.log(data);
          $('#subcats').empty();
          $('#subcats').append('<option value="0" disable="true" selected="true">=== Alt Kategori Seç ===</option>');

          $.each(data, function(index, subcatsObj){
            $('#subcats').append('<option value="'+ subcatsObj.id +'">'+ subcatsObj.name +'</option>');
          })
        });
	});

     </script>
	 <script src="{{URL::to('js/pickadate/legacy.js')}}"></script>
	 <script src="{{URL::to('js/pickadate/picker.js')}}"></script>
	 <script src="{{URL::to('js/pickadate/picker.date.js')}}"></script>
	 <script src="{{URL::to('js/pickadate/picker.time.js')}}"></script>
	 <script src="{{URL::to('js/pickadate/tr_TR.js')}}"></script>
	 <script src="{{URL::to('js/bootstrap-timepicker.min.js')}}"></script>
	 <script>
	 $(function() {
	  // Enable Pickadate on an input field and
	  // specifying date format for hidden input field
	  $('#date').pickadate({
		formatSubmit : 'yyyy/mm/dd 00:00:00',
		hiddenName : true
	  });
      $('#zaman').timepicker({
          minuteStep: 1,
          appendWidgetTo: 'body',
          showSeconds: true,
          showMeridian: false,
          defaultTime: false
      });
	 });
    $(document).ready(function() {
            $('.aciklama').richText();
            $('.genisAciklama').richText();
    });


    function readURL4imageName(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imageName-change').css('background-image', 'url('+e.target.result+')');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function readURL4image1(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#image1-change').css('background-image', 'url('+e.target.result+')');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            function readURL4image2(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#image2-change').css('background-image','url('+e.target.result+')');
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                function readURL4image3(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#image3-change').css('background-image', 'url('+e.target.result+')');
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
        $("#imageName-choose").change(function(){
            readURL4imageName(this);
        });
        $("#image1-choose").change(function(){
            readURL4image1(this);
        });
        $("#image2-choose").change(function(){
            readURL4image2(this);
        });
        $("#image3-choose").change(function(){
            readURL4image3(this);
        });

	</script>
@endsection
