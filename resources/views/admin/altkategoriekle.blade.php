@extends('layouts.master')


@section('title')
    Alt Kategori Ekleme Ekranı
@endsection


@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h1>Alt Kategori Ekle</h1>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.addSubCat') }}" method="post">
  <div class="form-group">
      <label for="name">Ana Kategori Seç</label>
      <select class="form-control" name="id">
        @foreach($cats as $cat)
        <option value="{{$cat->id}}">{{$cat->name}}</option>
        @endforeach
      </select>
  </div>
    <div class="form-group">
        <label for="name">Alt Kategori Adı</label>
        <input type="text" id="name" name="name" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Alt Kategori Ekle</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
