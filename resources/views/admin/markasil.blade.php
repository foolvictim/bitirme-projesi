@extends('layouts.master')

@section('title')
    Marka Silme Ekranı
@endsection

@section('content')
<div class="row">
  <div class="col-md-4 col-md-offset-4">
<h2>Marka Silme İşlemi</h2>
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
<form action="{{ route('admin.removeBrand') }}" method="post">
    <div class="form-group">
        <label for="name">Bu markayı gerçekten silmek istiyor musunuz?</label>
        <select name="id" >

<option value="{{$brand->id}}>">{{$brand->name}}</option>

        </select>
    </div>
    <button type="submit" class="btn btn-primary">Markayı Sil</button>
    {{csrf_field()}}
</form>

  </div>
</div>

@endsection
