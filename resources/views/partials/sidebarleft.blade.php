
					<div class="col-sm-3">
					<div class="left-sidebar">

<center>
@if (isset($subcatid) && $subcatid > 0)
						<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Ürünleri Sırala &nbsp;
					<i class="fa fa-sort" aria-hidden="true"></i>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="{{route('sort.artan', ['subcatid'=>$subcatid, 'markaid'=>0])}}"><i class="fa fa-sort-asc" aria-hidden="true"></i> &nbsp;Fiyata Göre Artan</a></li>
					<li><a href="{{route('sort.azalan', ['subcatid'=>$subcatid, 'markaid'=>0])}}"><i class="fa fa-sort-desc" aria-hidden="true"></i> &nbsp; Fiyata Göre Azalan</a></li>
				</ul>
			</div>
			<br>
@elseif (isset($markaid) && $markaid > 0)
						<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Ürünleri Sırala &nbsp;
					<i class="fa fa-sort" aria-hidden="true"></i>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="{{route('sort.artan', ['subcatid'=>0, 'markaid'=>$markaid])}}"><i class="fa fa-sort-asc" aria-hidden="true"></i> &nbsp;Fiyata Göre Artan</a></li>
					<li><a href="{{route('sort.azalan', ['subcatid'=>0, 'markaid'=>$markaid])}}"><i class="fa fa-sort-desc" aria-hidden="true"></i> &nbsp; Fiyata Göre Azalan</a></li>
				</ul>
			</div>
			<br>
@else
						<div class="dropdown">
				<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Ürünleri Sırala &nbsp;
					<i class="fa fa-sort" aria-hidden="true"></i>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="{{route('sort.artan', ['subcatid'=>0, 'markaid'=>0])}}"><i class="fa fa-sort-asc" aria-hidden="true"></i> &nbsp;Fiyata Göre Artan</a></li>
					<li><a href="{{route('sort.azalan', ['subcatid'=>0, 'markaid'=>0])}}"><i class="fa fa-sort-desc" aria-hidden="true"></i> &nbsp; Fiyata Göre Azalan</a></li>
				</ul>
			</div>
			<br>
@endif
		</center>
						<h2>Kategoriler</h2>
						<div class="panel-group category-products" id="accordian">
						@foreach($categories as $category)
							@if($category->subcats->count() > 0)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										@if(isset($activeCatId) && ($category->id == $activeCatId))
										<a id="tgt{{ $activeCatId }}" class="faruk" data-toggle="collapse" data-parent="#accordian" href="#{{ $category->id . 'cat' }}">
										@else
										<a data-toggle="collapse" data-parent="#accordian" href="#{{ $category->id . 'cat' }}">
										@endif
											<span class="badge pull-right"><i class="fa fa-plus"></i></span> {{ $category->name }}
										</a>
									</h4>
								</div>
								<div id="{{ $category->id . 'cat' }}" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											@foreach($category->subcats as $subcategory)
											<li><a href="{{ route('product.catindex', ['id' => $subcategory->id]) }}">{{ $subcategory->name }} </a></li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
							@else
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">{{ $category->name }}</a></h4>
								</div>
							</div>
							@endif
						@endforeach
						</div>

						<div class="brands_products">
							<h2>Markalar</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									@foreach($brands as $brand)
									<li><a href="{{ route('product.brands', ['id'=>$brand->id]) }}"> <span class="pull-right"></span>{{ $brand->name }}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
