

				<div class="col-sm-9 padding-right">
					<div class="row pull-right">
					</div>
					<div style="clear:both;"></div>
					<div class="features_items"><!--features_items-->

						<h2 class="title text-center">İndirimdeki Ürünler</h2>
						@if(Session::has('success'))
							 <div class="row">
								 <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
									 <div id="charge-message" class="alert alert-success">
										 {{ Session::get('success') }}
									 </div>
								 </div>
							 </div>
						@endif
						@if(Session::has('hata'))
						<div class="row">
							<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
								<div id="charge-message" class="alert alert-danger">
									{{ Session::get('hata') }}
								</div>
							</div>
						</div>
						@endif



						@if($products->count() > 0)
						@foreach($products as $product)
						<div class="col-sm-4">
							<div class="product-image-wrapper text-center">
								<div class="countdown">
									<h4>Kalan Adet:  {{ $product->qty }}</h4>
								</div>

								<div class="single-products">
									<div class="productinfo text-center">
										<div style="background-image: url('{{ route('product.image', ['filename' => $product->imageName]) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
										<!--<img src="{{ route('product.image', ['filename' => $product->imageName]) }}" alt="" /> -->
									</div>
										<h2 id="cp{{$product->id}}" class="currentpriceholder">{{ number_format($product->current_price, 2, ',', '.') }} TL</h2>
										<h4><del>{{ number_format($product->price, 2, ',', '.') }} TL</del></h4>
										<h4 id="etc{{$product->id}}">{{ $product->title }}</h4>
										<a id="atc{{$product->id}}" href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn btn-default add-to-cart addtocart"><i class="fa fa-shopping-cart"></i>Sepete Ekle</a><span id="atcart{{$product->id}}"></span>
									</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
									@if(isset(Auth::user()->id))
									@if(\App\Follow::where('product_id', $product->id)->where('user_id', Auth::user()->id)->first())
										<li><a id="tak{{ $product->id }}" class="addtotakip" href="#"><i class="fa fa-minus-square"></i>Takibi Bırak</a></li>
									@else
										<li><a id="tak{{ $product->id }}" class="addtotakip" href="#"><i class="fa fa-plus-square"></i>Takip Et</a></li>
									@endif
									@endif
										<li><a href="{{ route('product.details', ['id' => $product->id]) }}"><i class="fa fa-plus-square"></i>Detaylar</a></li>
									</ul>
								</div>
							</div>
						</div>
						@endforeach

						@else
						@if(isset($activatedCatId))
						<h1>Kategoride ürün bulunamadı.</h1>
						@else
						@if(Route::currentRouteName() == 'product.searchindex')
						<h1>"{{ Request::input('searchString') }}" aramasına uygun ürün bulunamadı.</h1>
						@else
						<h1>Ürün yok.</h1>
						@endif
						@endif
						@endif
					</div><!--features_items-->
					@if($products->count() > 0)
					{{ $products->links() }}
					@endif
				</div>
