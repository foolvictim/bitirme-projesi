@extends('layouts.master')

@section('title')
    Ürün Detayları
@endsection

@section('content')
@if(count($errors)>0)
<div class="alert alert-danger">
  @foreach($errors->all() as $error)
<p>{{$error}}</p>
@endforeach
</div>
@endif
@include('partials.sidebarleft')
<div class="col-sm-9 padding-right">
				<nav aria-label="breadcrumb" role="navigation">
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">{{$product->cat->name}}</a></li>
					<li class="breadcrumb-item"><a href="#">{{$product->subcat->name}}</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$product->title}}</li>
				  </ol>
				</nav>
<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
                <div class="detailedImage" style="background-image: url('{{ route('product.image', ['filename' => $product->imageName]) }}'); display: block; background-size: 300px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 450px;">
							<!--	<img src="{{ route('product.image', ['filename' => $product->imageName]) }}" width="200" height="200" alt="" /> -->
            </div>
							</div>


						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2>{{$product->title}}</h2>
								<img src="images/product-details/rating.png" alt="" />
								<span>
									<span id="atc{{$product->id}}" class="addtocart">
									<span id="cp{{$product->id}}" class="currentpriceholder">{{ number_format($product->current_price, 2, ',', '.') }} <i class="fa fa-try" aria-hidden="true"></i></span></span>
									<span style="color:black;"><del>{{ number_format($product->price, 2, ',', '.') }} TL</del> <i class="fa fa-try" aria-hidden="true"></i></span>
<hr><label></label>
									<form action="{{ route('product.addToCartQty') }}" method="post">
										{{ csrf_field() }}
    									<input type="text" name="qty" value="1" />
    									<input type="hidden" name="p_id" value="{{ $product->id }}" />
    									<button type="submit" class="btn btn-fefault cart">
    										<i class="fa fa-shopping-cart"></i>
    										Sepete Ekle
    									</button>
									</form>
								</span>
								<p><b>Stok Durumu:</b> Mevcut</p>
								<p><b>Durum:</b> Yeni</p>
								<p><b>Marka:</b> Marka</p>
@if(isset($following) && $following==true)

<form action="{{ route('unfollow') }}" method="post" id="follow-form">
  {{ csrf_field() }}
  <input type="hidden" id="id" name="product_id" class="form-control" value="{{$product->id}}">
<button type="submit" class="btn btn-danger">Takipten Çıkar</button>
</form>

@elseif(isset($following) && $following==false)
<form action="{{ route('follow') }}" method="post" id="follow-form">
  {{ csrf_field() }}
  <input type="hidden" id="id" name="product_id" class="form-control" value="{{$product->id}}">
<button type="submit" class="btn btn-success">Takip Et</button>
</form>
@else
<form action="{{ route('follow') }}" method="post" id="follow-form">
  {{ csrf_field() }}
  <input type="hidden" id="id" name="product_id" class="form-control" value="{{$product->id}}">
<button type="submit" class="btn btn-success">Takip Et</button>
</form>
@endif






								<a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
							</div><!--/product-information-->
						</div>
						<div id="similar-product" style="clear:both;margin: 0 auto;" class="carousel slide" data-ride="carousel">

								  <!-- Wrapper for slides -->
								    <div class="carousel-inner">
										<div class="item active">
                       <a href="javascript:void(0);"><img class="thumb" src="{{ route('product.image', ['filename' => $product->imageName]) }}" width="100" height="100" alt=""></a>
										  <a href="javascript:void(0);"><img class="thumb" src="{{ route('product.image', ['filename' => $product->image1]) }}" width="100" height="100" alt=""></a>
										  <a href="javascript:void(0);"><img class="thumb" src="{{ route('product.image', ['filename' => $product->image2]) }}" width="100" height="100" alt=""></a>
										  <a href="javascript:void(0);"><img class="thumb" src="{{ route('product.image', ['filename' => $product->image3]) }}" width="100" height="100" alt=""></a>
										</div>

									</div>

								  <!-- Controls -->

						</div>
					</div><!--/product-details-->

					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								@if(isset($_GET['page']) && $_GET['page']>0)
								<li><a href="#details" data-toggle="tab">Ürün Detayları</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Diğer Ürünler</a></li>
								<li class="active"><a href="#reviews" data-toggle="tab">Ürün Yorumları ({{$product->comments->count()}})</a></li>
								@else
								<li class="active"><a href="#details" data-toggle="tab">Ürün Detayları</a></li>
								<li><a href="#companyprofile" data-toggle="tab">Diğer Ürünler</a></li>
								<li><a href="#reviews" data-toggle="tab">Ürün Yorumları ({{$product->comments->count()}})</a></li>
								@endif
							</ul>
						</div>
						<div class="tab-content">
							@if(!isset($_GET['page']))
							<div class="tab-pane fade active in" id="details" >
							@else
							<div class="tab-pane fade in" id="details" >
							@endif
								<p>{!! $product->rich_description !!}</p>
							</div>

							<div class="tab-pane fade" id="companyprofile" >
@php
$randomProducts = \App\Product::orderBy(DB::raw('RAND()'))->take(4)->where('durum', 1)->where('id', '!=', $product->id)->get();

@endphp
@foreach($randomProducts as $sliderProduct)

								<div class="col-sm-3">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
                        <div style="background-image: url('{{ route('product.image', ['filename' => $sliderProduct->imageName]) }}'); display: block; background-size: 170px auto; background-repeat: no-repeat; background-position: center; width: auto; height: 250px;">
											<!--	<img src="{{ route('product.image', ['filename' => $sliderProduct->imageName]) }}" alt="" /> -->
                      </div>
												<h2 id="atc{{$sliderProduct->id}}" class="addtocart"><span id="cp{{$sliderProduct->id}}">{{ number_format($sliderProduct->current_price, 2, ',', '.') }} TL</span></h2>
												<p>{{ $sliderProduct->title }}</p>
												<a href="{{ route('product.addToCart', ['id' => $sliderProduct->id]) }}" class="btn btn-default add-to-cart"><button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button></a>
											</div>
										</div>
									</div>
								</div>

@endforeach

							</div>

							@if(isset($_GET['page']))
							<div class="tab-pane active fade in" id="reviews" >
							@else
							<div class="tab-pane fade" id="reviews" >
							@endif
								<div class="col-sm-12">
									@foreach($comments as $comment)
									<ul>
										<li><a href=""><i class="fa fa-user"></i>{{ $comment->name }}</a></li>
										<li><a href=""><i class="fa fa-clock-o"></i>{{ $comment->created_at->toTimeString()}}</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>{{ $comment->created_at->toDateString()}}</a></li>
									</ul>
									<p>{{ $comment->description }}</p>
									@endforeach
									<hr>

									<form action="{{ route('postcomment') }}" method="post">
										{{ csrf_field() }}<span>
    									<input type="text" name="ad" placeholder="Adınız"/>
    									<input type="text" name="email" placeholder="E-mail Adresiniz"/></span>
    									<textarea name="description" placeholder="Yorumunuz..."></textarea>
    									<input type="hidden" name="prd_id" value="{{ $product->id }}" />
    									<button type="submit" class="btn btn-fefault cart">Gönder</button>
									</form>
									{{ $comments->links() }}
								</div>
							</div>
						</div>
					</div><!--/category-tab-->

				</div>
@endsection
@section('scripts')
@include('partials.listescripti')
<script>
    var img;
    $(".thumb").click(function(e) {
        e.preventDefault();
        img = $(this).attr("src");
        $(".detailedImage").css('background-image', 'url("' + img + '")');

    });
</script>
@endsection
